var cardTeams;
var startX;
var startY;
var endX;
var endY;
var key;

function isInitLocation(data){
    for(var k in data){
        if (data[k].right != 0) {
            data[k].right =0 ;
            return false ;
        }
    }

    return true ;
}

Page({

    onLoad: function() {
        cardTeams = [{
            id: 'simgle',
            right: 0
        }];
        this.setData({
            cardTeams: cardTeams
        });

    },

    /**
     *
     *
     */
    drawerStart: function(e) {
        //假如已经处于左滑位置，则不能滑动并且重置位置
        if (isInitLocation(cardTeams)) {
            var touch = e.touches[0];
            startX = touch.clientX;
            startY = touch.clientY;
            key = true;
        }else{
            this.setData({
                cardTeams: cardTeams
            });
            key=false ;
            //本想阻止事件冒泡，不过貌似做不到
        }


    },

    drawerEnd: function(e) {
        var self = this;
        var dataId = e.currentTarget.id;
        if (key) {
            var Xnum = (endX - startX) < 0 ? startX - endY : endX - startX;
            var Ynum = (endY - startY) < 0 ? startY - endY : endY - startY;
            if (Xnum >= Ynum) {
                var res = cardTeams;
                if (endX - startX <= -5) {
                    for (var k in res) {
                        var data = res[k];
                        if (res[k].id == dataId) {
                            res[k]["right"] = "15%";

                        } else {
                            res[k]["right"] = 0;
                        }
                        key = false;
                    }

                } else {
                    for (var k in res) {
                        var data = res[k];
                        if (res[k].id == dataId) {
                            res[k]["right"] = "0";
                        }

                        key = false;
                    }
                }
            }else{
                isInitLocation(cardTeams) ;
            }

        }

        self.setData({
            cardTeams: cardTeams
        });

    },

    /**
     * 触碰滑动的事件
     */
    drawerMove: function(e) {
        //由于bindtouchend无法获取到最后离开的坐标，所以只能够通过覆盖滑动的值渠道最后离开的位置
        var touch = e.touches[0];
        endX = touch.clientX;
        endY = touch.clientY;
    }




})
