function formatTime( date ) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds();


  return [ year, month, day ].map( formatNumber ).join( '/' ) + ' ' + [ hour, minute, second ].map( formatNumber ).join( ':' )
}

function formatNumber( n ) {
  n = n.toString()
  return n[ 1 ] ? n : '0' + n
}




/**
* 页面跳转，保留当前页，跳转到应用内的某个页面
* url 跳转目标连接地址
* successFun  调用成功回调函数
* failFun 调用失败回调函数
* completeFun 调用节后后回调函数(不管成功失败都调用)
*/

function vavigateTo( url, successFun, failFun, completeFun ) {
  wx.navigateTo( {
    url: url,
    success: function( date ) {
      if( successFun ) {
        successFun();
      }
    },
    fail: function( date ) {
      if( failFun ) {
        failFun();
      }
    },
    complete: function( date ) {
      if( completeFun ) {
        completeFun();
      }
    },
  });
}

/**
* 返回前一页
*/
function navigateBack() {
  wx.navigateBack();
}




//====================================网络访问Util==========================
/**
* 模拟简单的请求效果，其中默认值有 ：
*    header:{
*       "Content-Type":"application/json"
*    }
*    method : GET
*    
*   参数为:
*      resposeData :返回的数据
*      url :请求url地址
*      paramData :请求参数 (json)
*      url :请求url地址
*      
*/
function request( resposeData, url, paramData ) {
  wx.request( {
    url: url,
    data: paramData ? paramData : {},
    header: {
      "Content-Type": "application/json"
    },
    success: function( res ) {
      console.info( "123123123" );
      console.info( "data:" + res.data );
      resposeData = res.data;
    },
    fail: function( res ) {
      console.info( res );
      console.info( "fail" );
    }
  });
}


/**
* 将对象转化为uri
*/
function _obj2uri( obj ) {
  return Object.keys( obj ).map( function( k ) {
    return encodeURIComponent( k ) + "=" + encodeURIComponent( obj[ k ] );
  }).join( '&' );
}


module.exports = {
  formatTime: formatTime,
  vavigateTo: vavigateTo,
  navigateBack: navigateBack,
  request: request
}
