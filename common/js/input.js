//input.js 

var commonUtil = require('../../utils/util.js')

function openSearchPage(url){
	commonUtil.vavigateTo(url) ;
}

function  backPrePage (){
	commonUtil.navigateBack() ;
}

module.exports ={
	openSearchPage:openSearchPage  ,
	backPrePage:backPrePage
}