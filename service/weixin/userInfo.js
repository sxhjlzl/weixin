function getWeixinChartInfo(){
	var data = {
		hiddenLoading :true ,
		userInfos :[
		{
			"image":"../../image/weixin/userImage/1.jpg" ,
			"markName":"燕燕" ,
			"nickName" :"烧不尽的荒唐" ,
			"message" : [
				{
					"messageText" : "hello" ,
					"messageTime" : "上午 10:05"
				},
				{
					"messageText" : "我爱你" ,
					"messageTime" : "上午 10:03"
				},
				{
					"messageText" : "我爱你" ,
					"messageTime" : "上午 10:02"
				}
			]
		},
		{
			"image":"../../image/weixin/userImage/2.jpg" ,
			"markName":"" ,
			"nickName" :"烧不尽的荒唐" ,
			"message" : [
				{
					"messageText" : "好吧" ,
					"messageTime" : "上午 10:05"
				},
				{
					"messageText" : "我爱你" ,
					"messageTime" : "上午 10:03"
				},
				{
					"messageText" : "我爱你" ,
					"messageTime" : "上午 10:02"
				}
			]
		},
		{
			"image":"../../image/weixin/userImage/3.jpg" ,
			"markName":"楚楚" ,
			"nickName" :"烧不尽的荒唐" ,
			"message" : [
				{
					"messageText" : "你中午去吃饭吗" ,
					"messageTime" : "上午 10:05"
				},
				{
					"messageText" : "我爱你" ,
					"messageTime" : "上午 10:03"
				},
				{
					"messageText" : "我爱你" ,
					"messageTime" : "上午 10:02"
				}
			]
		},
		{
			"image":"../../image/weixin/userImage/4.jpg" ,
			"markName":"UU" ,
			"nickName" :"烧不尽的荒唐" ,
			"message" : [
				{
					"messageText" : "UU要吃饭" ,
					"messageTime" : "上午 10:05"
				},
				{
					"messageText" : "我爱你" ,
					"messageTime" : "上午 10:03"
				},
				{
					"messageText" : "我爱你" ,
					"messageTime" : "上午 10:02"
				}
			]
		}
		]
	}
	return data ;
}


module.exports = {
  getWeixinChartInfo:getWeixinChartInfo 
}
